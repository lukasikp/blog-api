module.exports = {
  apps: [
    {
      name: "API",
      script: "server.js",

      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      args: "one two",
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env_production: {
        port: 80,
        NODE_ENV: "production"
      }
    }
  ],

  deploy: {
    production: {
      user: "root",
      host: "51.68.140.134",
      ref: "origin/master",
      repo: "git@bitbucket.org:lukasikp/blog-api.git",
      path: "/srv/rest/",
      "post-deploy": "yarn install && pm2 startOrRestart ecosystem.config.js"
    }
  }
};
