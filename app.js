const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
var path = require("path");

const articlesRoutes = require("./api/routes/articles");
const categoriesRoutes = require("./api/routes/categories");
const tagsRoutes = require("./api/routes/tags");
const userRoutes = require("./api/routes/user");
const uploadsRoutes = require("./api/routes/uploads");
const pagesRoutes = require("./api/routes/pages");
const navbarLinksRoutes = require("./api/routes/navbarLinks");
const debug = require("debug")("dev");

mongoose
  // .connect("mongodb://3.122.124.79:27017/admin", {
  //   user: "admin3648kl7",
  //   pass: "pass76e84p",
  //   useNewUrlParser: true,
  //   useMongoClient: true
  // })
  .connect("mongodb://127.0.0.1:27017/blog", {
    useNewUrlParser: true
  })
  .then(res => {
    debug("Connected to Database Successfully.");
  })
  .catch(() => {
    debug("Conntection to database failed.");
  });

app.use(morgan("dev"));

app.use("/site", express.static(path.resolve(__dirname, "site/build/")));

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/site/build/index.html");
});

app.use(express.static(__dirname + "/site/build"));
app.use("/admin", express.static(__dirname + "/admin/build"));
// app.use("/admin", express.static(path.join(__dirname, "/admin/build")));

// app.get("/admin", function(req, res) {
//   res.sendFile(path.join(__dirname, "/admin/build", "index.html"));
// });

app.use("/uploads", express.static("uploads"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// app.use((req, res, next) => {

//     res.header('Access-Control-Allow-Origin', 'http://localhost:3001');
//     res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

//     if (req.method === 'OPTIONS') {
//         res.header('Access-Control-Allow-Method', 'PUT, POST, PATCH, DELETE, GET');
//         return res.status(200).json({'message': 'done'});
//     }

//     next();
// })

app.use("/articles", articlesRoutes);
app.use("/categories", categoriesRoutes);
app.use("/tags", tagsRoutes);
app.use("/user", userRoutes);
app.use("/uploads", uploadsRoutes);
app.use("/pages", pagesRoutes);
app.use("/navbar-links", navbarLinksRoutes);

app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
