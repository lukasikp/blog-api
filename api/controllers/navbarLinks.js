const mongoose = require("mongoose");
const debug = require("debug")("dev");

const NavbarLink = require("../models/navbarLink");

exports.navbar_links_get_all = (req, res, next) => {
  NavbarLink.find()
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        result: docs
      };
      debug(docs);
      if (docs.length >= 0) {
        res.status(200).json(response);
      } else {
        res.status(404).json({ message: "No entries found" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err, message: "Server error" });
    });
};

exports.navbar_links_create_link = (req, res, next) => {
  const length = NavbarLink.count();
  const link = new NavbarLink({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    url: req.body.url,
    order: req.body.order || length
  });
  link
    .save()
    .then(result => {
      debug(result);
      res.status(201).json({
        message: "Handling POST request to /navbar-links",
        createdCategory: {
          name: result.name,
          url: result.url,
          order: result.order,
          _id: result._id,
          request: {
            type: "GET",
            url: "http://localhost:3000/categories/" + result._id
          }
        }
      });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err, message: "Server error" });
    });
};

exports.navbar_links_delete_link = (req, res, next) => {
  const id = req.params.linkId;
  NavbarLink.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({ message: "Navbar link deleted from collection" });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err, message: "Server error" });
    });
};
