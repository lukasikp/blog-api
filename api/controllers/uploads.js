const mongoose = require("mongoose");
const url = require("url");
const fs = require("fs");
const { lstatSync, readdirSync } = require("fs");
const { promisify } = require("util");
const { join } = require("path");
const debug = require("debug")("dev");

const unlinkAsync = promisify(fs.unlink);

const imageFolder = "./uploads/";

// localhost:3000/uploads?offset=20;
//limit =
exports.uploads_get_all = (req, res, next) => {
  fs.readdir(imageFolder, (err, files) => {
    const list = files.map(file => `http://localhost:3000/uploads/${file}`);
    return res.status(201).json({
      message: "Handling GET request to /uploads",
      result: list,
      request: {
        type: "GET",
        image: "http://localhost:3000/uploads"
      }
    });
  });
};

exports.uploads_get_months_directiories = (req, res, next) => {
  const year = req.params.year;
  fs.readdir(`${imageFolder}${year}/`, (err, files) => {
    const list = files.map(
      file => `http://localhost:3000/uploads/${year}/${file}/`
    );
    return res.status(201).json({
      message: `Handling GET request to /uploads/${year}/`,
      result: list,
      request: {
        type: "GET",
        url: `http://localhost:3000/uploads/${year}/`
      }
    });
  });
};

exports.uploads_get_images = (req, res, next) => {
  const year = req.params.year;
  const month = req.params.month;
  fs.readdir(`${imageFolder}${year}/${month}/`, (err, files) => {
    const list = files.map(
      file => `http://localhost:3000/uploads/${year}/${month}/${file}`
    );
    return res.status(201).json({
      message: `Handling GET request to /uploads/${year}/${month}/`,
      result: list,
      request: {
        type: "GET",
        url: `http://localhost:3000/uploads/${year}/${month}/`
      }
    });
  });
};

exports.uploads_add_file = (req, res, next) => {
  return res.status(201).json({
    message: "Handling POST request to /uploads",
    uploaded: {
      result: {
        image: req.file.path
      },
      request: {
        type: "POST",
        image: "http://localhost:3000/uploads"
      }
    }
  });
};

exports.uploads_delete_file = (req, res, next) => {
  const url = `./uploads${req.url}`;
  unlinkAsync(url)
    .then(() => {
      res.status(200).json({ message: "Image deleted" });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err, message: "Server error" });
    });
};
