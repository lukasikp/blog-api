const mongoose = require("mongoose");
const helpers = require("../helpers/request-helpers");
const url = require("url");
const debug = require("debug")("dev");

const Article = require("../models/article");
const Category = require("../models/category");

const paginateOptions = {};

// http://localhost:3000/articles/?offset=2&limit=2
// queries:
//offset=number;
//limit= number;
//published= bolean
//category= category's id

exports.articles_get_all = (req, res, next) => {
  const url_parse = url.parse(req.url, true);
  const limit = Number(url_parse.query.limit) || 20;
  const offset = Number(url_parse.query.offset) || 0;
  const category = url_parse.query.category || null;
  const published = url_parse.query.published || null;

  const filterOptions = helpers.create_filter_options(category, published);
  Article.paginate(filterOptions, {
    offset: offset,
    limit: limit,
    select: "title url content category articleImage _id publishedAt lead",
    sort: { createdAt: -1 },
    populate: { path: "category", select: "name color" }
  })
    .then(resp => {
      const response = {
        count: resp.docs.length,
        previous: helpers.create_pev_url(req.url, limit, offset, category),
        next: helpers.create_next_url(
          req.url,
          limit,
          offset,
          resp.total,
          category
        ),
        total: resp.total,
        offset: resp.offset,
        result: resp.docs.map(doc => {
          return {
            title: doc.title,
            lead: doc.lead,
            url: doc.url,
            content: doc.content,
            category: doc.category,
            _id: doc._id,
            articleImage: doc.articleImage,
            publishedAt: doc.publishedAt,
            request: {
              type: "GET",
              url: "http://localhost:3000/articles/" + doc._id
            }
          };
        })
      };

      if (resp.docs.length >= 0) {
        res.status(200).json(response);
      } else {
        res.status(404).json({ message: "No entries found" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.articles_create_article = (req, res, next) => {
  Category.findById(req.body.categoryId)
    .then(category => {
      if (!category) {
        return res.status(404).json({
          message: "category doesn't exist"
        });
      }
      const article = new Article({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        lead: req.body.lead,
        url: req.body.url,
        content: req.body.content,
        category: req.body.categoryId,
        articleImage: req.file.path,
        createdAt: new Date(),
        publishedAt: req.body.publishedAt || "",
        published: req.body.publishedAt ? true : false
      });
      article.save().then(result => {
        return res.status(201).json({
          message: "Handling POST request to /articles",
          userMessage: "Dodano nowy artykuł",
          createdArticle: {
            result: result,
            request: {
              type: "GET",
              url: "http://localhost:3000/articles/" + result._id
            }
          }
        });
      });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.articles_get_article = (req, res, next) => {
  const id = req.params.articleId;
  Article.findById(id)
    .populate("category", "name")
    .exec()
    .then(doc => {
      if (doc) {
        res.status(200).json({
          result: doc,
          request: {
            type: "GET",
            url: `http://localhost:3000/articles/${id}`
          }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.articles_update_article = (req, res, next) => {
  const id = req.params.articleId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  Article.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "article updated",
        userMessage: "Zapisano zmiany",
        request: {
          type: "GET",
          url: "http://localhost:3000/articles/" + id
        }
      });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.articles_delete_article = (req, res, next) => {
  const id = req.params.articleId;
  Article.remove({ _id: id })
    .exec()
    .then(result => {
      res.json({ message: "Product deleted" });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
};
