const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const debug = require("debug")("dev");
const User = require("../models/user");

exports.user_signup = (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          message: "Mail exists"
        });
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              email: req.body.email,
              password: hash
            });
            user
              .save()
              .then(result => {
                debug(result);
                res.status(201).json({
                  message: "User created"
                });
              })
              .catch(err => {
                debug(err);
                res.status(500).json({
                  error: err
                });
              });
          }
        });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.user_login = (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: "Auth failed"
        });
      } else {
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
          if (err) {
            return res.status(401).json({
              message: "Auth failed"
            });
          }
          if (result) {
            const token = jwt.sign(
              {
                email: user[0].email,
                userId: user[0]._id
              },
              process.env.JWT_KEY
              // {
              //     expiresIn: "1h"
              // },
            );
            return res.status(200).json({
              message: "Auth successful",
              token: token,
              userId: user[0]._id
            });
          }
          return res.status(401).json({
            message: "Auth failed"
          });
        });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.user_get_all = (req, res, next) => {
  User.find()
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        result: docs.map(doc => {
          return {
            email: doc.email,
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/user/" + doc._id
            }
          };
        })
      };
      debug(docs);
      if (docs.length >= 0) {
        res.status(200).json(response);
      } else {
        res.status(404).json({ message: "No entries found" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.user_get_data = (req, res, next) => {
  const id = req.params.userId;
  User.findById(id)
    .exec()
    .then(doc => {
      debug(doc);
      if (doc) {
        res.status(200).json({
          result: {
            name: doc.name,
            surname: doc.surname,
            nick: doc.nick,
            email: doc.email,
            description: doc.description,
            image: doc.image,
            bgImage: doc.bgImage,
            _id: doc._id
          },
          request: {
            type: "GET",
            url: `http://localhost:3000/user/${id}`
          }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.user_update_data = (req, res, next) => {
  const id = req.params.userId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  User.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      debug(result);
      res.status(200).json({
        message: "user updated",
        request: {
          type: "GET",
          url: "http://localhost:3000/user/" + id
        }
      });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.user_delete = (req, res, next) => {
  User.remove({ _id: req.params.userId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "User deleted"
      });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};
