const mongoose = require("mongoose");
const helpers = require("../helpers/request-helpers");
const url = require("url");
const debug = require("debug")("dev");

const Page = require("../models/page");

const paginateOptions = {};

// http://localhost:3000/pages/?offset=2&limit=2
// queries:
//offset=number;
//limit= number;
//published= bolean

exports.pages_get_all = (req, res, next) => {
  const url_parse = url.parse(req.url, true);
  const limit = Number(url_parse.query.limit) || 20;
  const offset = Number(url_parse.query.offset) || 0;
  const published = url_parse.query.published || null;

  const filterOptions = helpers.published_filter(published);
  Page.paginate(filterOptions, {
    offset: offset,
    limit: limit,
    select: "name url content _id publishedAt",
    sort: { createdAt: -1 }
  })
    .then(resp => {
      const response = {
        count: resp.docs.length,
        previous: helpers.create_pev_pages_url(req.url, limit, offset),
        next: helpers.create_next_pages_url(req.url, limit, offset, resp.total),
        total: resp.total,
        offset: resp.offset,
        result: resp.docs
      };

      if (resp.docs.length >= 0) {
        res.status(200).json(response);
      } else {
        res.status(404).json({ message: "No entries found" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.pages_create_page = (req, res, next) => {
  const page = new Page({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    url: req.body.url,
    content: req.body.content,
    createdAt: new Date(),
    publishedAt: req.body.publishedAt || "",
    published: req.body.publishedAt ? true : false
  });
  page.save().then(result => {
    debug(res);
    return res.status(201).json({
      message: "Handling POST request to /pages",
      userMessage: "Dodano nową stronę",
      createdPage: {
        result: result,
        request: {
          type: "GET",
          url: "http://localhost:3000/pages/" + result._id
        }
      }
    });
  });
};

exports.pages_get_page = (req, res, next) => {
  const id = req.params.pageId;
  Page.findById(id)
    .exec()
    .then(doc => {
      debug(doc);
      if (doc) {
        res.status(200).json({
          result: doc,
          request: {
            type: "GET",
            url: `http://localhost:3000/pages/${id}`
          }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.pages_update_page = (req, res, next) => {
  const id = req.params.pageId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  Page.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      debug(result);
      res.status(200).json({
        message: "page updated",
        userMessage: "Zapisano zmiany",
        request: {
          type: "GET",
          url: "http://localhost:3000/pages/" + id
        }
      });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.pages_delete_page = (req, res, next) => {
  const id = req.params.pageId;
  Page.remove({ _id: id })
    .exec()
    .then(result => {
      res.json({ message: "Page deleted" });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
};
