const mongoose = require("mongoose");
const debug = require("debug")("dev");

const Category = require("../models/category");

exports.categories_get_all = (req, res, next) => {
  Category.find()
    .select("name _id relations active color")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        result: docs.map(doc => {
          return {
            name: doc.name,
            _id: doc._id,
            relations: doc.relations,
            active: doc.active,
            color: doc.color,
            request: {
              type: "GET",
              url: "http://localhost:3000/categories/" + doc._id
            }
          };
        })
      };
      debug(docs);
      if (docs.length >= 0) {
        res.status(200).json(response);
      } else {
        res.status(404).json({ message: "No entries found" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err, message: "Server error" });
    });
};

exports.categories_create_category = (req, res, next) => {
  Category.find({ name: req.body.name })
    .exec()
    .then(findCategory => {
      if (findCategory.length >= 1) {
        return res.status(409).json({
          message: "Category exists"
        });
      } else {
        const category = new Category({
          _id: new mongoose.Types.ObjectId(),
          name: req.body.name,
          relations: {
            main_category: req.body.main_category === "false" ? false : true,
            parent_category: req.body.parent_category || null
          },
          color: req.body.color,
          active: req.body.active === false ? false : true
        });
        category
          .save()
          .then(result => {
            debug(result);
            res.status(201).json({
              message: "Handling POST request to /Categories",
              createdCategory: {
                name: result.name,
                _id: result._id,
                relations: {
                  main_category: result.relations.main_category,
                  parent_category: result.relations.parent_category
                },
                color: result.color,
                active: result.active,
                request: {
                  type: "GET",
                  url: "http://localhost:3000/categories/" + result._id
                }
              }
            });
          })
          .catch(err => {
            debug(err);
            res.status(500).json({ error: err, message: "Server error" });
          });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err, message: "Server error" });
    });
};

exports.categories_get_category = (req, res, next) => {
  const id = req.params.categoryId;
  Category.find({ _id: id })
    .exec()
    .then(doc => {
      if (doc) {
        res.status(200).json({
          result: doc,
          request: {
            type: "GET",
            url: `http://localhost:3000/categories/${id}`
          }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err });
    });
};

exports.categories_edit_category = (req, res, next) => {
  const id = req.params.categoryId;
  const body = req.body;
  const updateOps = {};
  body.name !== undefined ? (updateOps.name = body.name) : null;
  body.color !== undefined ? (updateOps.color = body.color) : null;
  updateOps.relations = {};
  body.main_category !== undefined
    ? (updateOps.relations.main_category = body.main_category)
    : null;
  body.parent_category !== undefined && body.parent_category !== ""
    ? (updateOps.relations.parent_category = body.parent_category)
    : null;
  body.active !== undefined ? (updateOps.active = body.active) : null;

  Category.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      debug(result);
      res.status(200).json({
        message: "Category updated",
        request: {
          type: "GET",
          url: "http://localhost:3000/categories/" + id
        }
      });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.categories_delete_category = (req, res, next) => {
  const id = req.params.categoryId;
  Category.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({ message: "Category deleted" });
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err, message: "Server error" });
    });
};
