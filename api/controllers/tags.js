const mongoose = require("mongoose");
const debug = require("debug")("dev");

const Tag = require("../models/tag");

exports.tags_get_all = (req, res, next) => {
  Tag.find()
    .select("name _id")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        result: docs.map(doc => {
          return {
            name: doc.name,
            _id: doc._id
          };
        }),
        request: {
          type: "GET",
          url: "http://localhost:3000/tags/"
        }
      };
      if (docs.length >= 0) {
        res.status(200).json(response);
      } else {
        res.status(404).json({ message: "No entries found" });
      }
    })
    .catch(err => {
      debug(err);
      res.status(500).json({ error: err, message: "Server error" });
    });
};

exports.tags_create_tag = (req, res, next) => {
  tagsArray = req.body.tags.split(",");
  let createdTags = [];
  for (const tag of tagsArray) {
    Tag.find({ name: tag })
      .exec()
      .then(findTag => {
        if (findTag.length === 0) {
          const newTag = new Tag({
            _id: new mongoose.Types.ObjectId(),
            name: tag
          });
          newTag
            .save()
            .then(result => {
              debug(result);
            })
            .catch(err => {
              debug(err);
              res.status(500).json({
                error: err,
                message: "Server error in saving single tag"
              });
            });
        }
      })
      .catch(err => {
        debug(err);
        res.status(500).json({ error: err, message: "Server error" });
      });
  }

  res.status(201).json({
    message: "Handling POST request to /Tags"
  });
};
