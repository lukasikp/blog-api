const express = require("express");
const cors = require("cors");
const router = express.Router();

const corsOptions = {
  origin: process.env.DEBUG == "dev" ? "*" : "http://trendnakulture.pl",
  methods: "PUT, POST, PATCH, DELETE, GET, OPTIONS",
  allowedHeaders:
    "Origin, X-Requested-With, Content-Type, Accept, Authorization",
  credentials: true
};
router.all("*", cors(corsOptions));

const checkAuth = require("../middleware/check-auth");
const UserController = require("../controllers/user");

router.post("/signup", UserController.user_signup);

router.post("/login", UserController.user_login);

router.get("/", checkAuth, UserController.user_get_all);

router.get("/:userId", UserController.user_get_data);

router.patch("/:userId", checkAuth, UserController.user_update_data);

router.delete("/:userId", checkAuth, UserController.user_delete);

module.exports = router;
