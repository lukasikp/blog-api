const express = require("express");
const cors = require("cors");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");

const CategoriesControllers = require("../controllers/categories");
const corsOptions = {
  origin: process.env.DEBUG === "dev" ? "*" : "http://trendnakulture.pl",
  methods: "PUT, POST, PATCH, DELETE, GET, OPTIONS",
  allowedHeaders:
    "Origin, X-Requested-With, Content-Type, Accept, Authorization",
  credentials: true
  // origin: function (origin, callback) {
  //     if (whitelist.indexOf(origin) !== -1) {
  //       callback({}, true)
  //     } else {
  //       callback(new Error('Not allowed by CORS'))
  //     }
  //   }
};

var corsOptions1 = {
  origin: function(origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  }
};

router.all("*", cors(corsOptions));
router.get("/", CategoriesControllers.categories_get_all);

router.post("/", checkAuth, CategoriesControllers.categories_create_category);

router.get("/:categoryId", CategoriesControllers.categories_get_category);

router.patch(
  "/:categoryId",
  checkAuth,
  CategoriesControllers.categories_edit_category
);

router.delete(
  "/:categoryId",
  checkAuth,
  CategoriesControllers.categories_delete_category
);

module.exports = router;
