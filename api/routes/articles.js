const express = require("express");
const cors = require("cors");
const router = express.Router();
const multer = require("multer");
const checkAuth = require("../middleware/check-auth");
const fs = require("fs");

const ArticlesController = require("../controllers/articles");

buldImagesPath = () => {
  const date = new Date();
  var dirYear = `./uploads/${date.getFullYear()}`;
  var dirFull = `./uploads/${date.getFullYear()}/${date.getMonth() + 1}`;
  if (!fs.existsSync(dirYear)) {
    fs.mkdirSync(dirYear);
  }
  if (!fs.existsSync(dirFull)) {
    fs.mkdirSync(dirFull);
  }
  return dirFull;
};

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, buldImagesPath());
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, "-") + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "image/png"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  fileFilter: fileFilter
});

const corsOptions = {
  origin: process.env.DEBUG === "dev" ? "*" : "http://trendnakulture.pl",
  methods: "PUT, POST, PATCH, DELETE, GET, OPTIONS",
  allowedHeaders:
    "Origin, mode, Access-Control-Allow-Origin, X-Requested-With, Content-Type, Accept, Authorization",
  credentials: true
};

router.all("*", cors(corsOptions));

router.get("/", ArticlesController.articles_get_all);

router.post(
  "/",
  cors(corsOptions),
  checkAuth,
  upload.single("articleImage"),
  ArticlesController.articles_create_article
);

router.get("/:articleId", ArticlesController.articles_get_article);

router.patch(
  "/:articleId",
  checkAuth,
  ArticlesController.articles_update_article
);

router.delete(
  "/:articleId",
  checkAuth,
  ArticlesController.articles_delete_article
);

module.exports = router;
