const express = require("express");
const cors = require("cors");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");

const NavbarLinksControllers = require("../controllers/navbarLinks");
const corsOptions = {
  origin: process.env.DEBUG === "dev" ? "*" : "http://trendnakulture.pl",
  methods: "PUT, POST, PATCH, DELETE, GET, OPTIONS",
  allowedHeaders:
    "Origin, X-Requested-With, Content-Type, Accept, Authorization",
  credentials: true
  // origin: function (origin, callback) {
  //     if (whitelist.indexOf(origin) !== -1) {
  //       callback({}, true)
  //     } else {
  //       callback(new Error('Not allowed by CORS'))
  //     }
  //   }
};

var corsOptions1 = {
  origin: function(origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  }
};

router.all("*", cors(corsOptions));
router.get("/", NavbarLinksControllers.navbar_links_get_all);

router.post("/", checkAuth, NavbarLinksControllers.navbar_links_create_link);

router.delete(
  "/:navbarLinksId",
  checkAuth,
  NavbarLinksControllers.navbar_links_delete_link
);

module.exports = router;
