const express = require("express");
const cors = require("cors");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const fs = require("fs");

const PagesController = require("../controllers/pages");

const corsOptions = {
  origin: process.env.DEBUG === "dev" ? "*" : "http://trendnakulture.pl",
  methods: "PUT, POST, PATCH, DELETE, GET, OPTIONS",
  allowedHeaders:
    "Origin, mode, Access-Control-Allow-Origin, X-Requested-With, Content-Type, Accept, Authorization",
  credentials: true
};

router.all("*", cors(corsOptions));

router.get("/", PagesController.pages_get_all);

router.post(
  "/",
  cors(corsOptions),
  checkAuth,
  PagesController.pages_create_page
);

router.get("/:pageId", PagesController.pages_get_page);

router.patch("/:pageId", checkAuth, PagesController.pages_update_page);

router.delete("/:pageId", checkAuth, PagesController.pages_delete_page);

module.exports = router;
