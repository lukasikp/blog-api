const mongoose = require("mongoose");

const navbarLinkSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
  url: { type: String, required: true },
  order: { type: Number, required: true }
});

module.exports = mongoose.model("navbarLinks", navbarLinkSchema);
