const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
  color: { type: String },
  relations: {
    main_category: { type: Boolean, default: true },
    parent_category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
      default: null
    }
  },
  active: { type: Boolean, default: true }
});

module.exports = mongoose.model("Category", categorySchema);
