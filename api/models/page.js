const mongoose = require("mongoose");
var mongoosePaginate = require("mongoose-paginate");

const pageSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
  url: { type: String, required: true, unique: true },
  content: String,
  createdAt: { type: Date, required: true },
  publishedAt: { type: Date },
  published: { type: Boolean }
});

pageSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Page", pageSchema);
