const mongoose = require("mongoose");
var mongoosePaginate = require("mongoose-paginate");

const articleSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  title: { type: String, required: true },
  lead: { type: String, required: true },
  url: { type: String, required: true, unique: true },
  category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
  subcategory: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
  tags: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Tag"
    }
  ],
  content: String,
  articleImage: { type: String },
  createdAt: { type: Date, required: true },
  publishedAt: { type: Date },
  published: { type: Boolean }
});

articleSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Article", articleSchema);
