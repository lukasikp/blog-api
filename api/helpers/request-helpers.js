const url = require("url");

exports.create_filter_options = (category, published) => {
  let filterOptions = {};
  category !== null
    ? (filterOptions["category"] = {
        _id: category
      })
    : null;
  published === null ? (filterOptions["published"] = true) : null;
  return filterOptions;
};

exports.create_pev_url = (requestUrl, limit, offset, category) => {
  let nextUrl;
  if (offset === 0) {
    nextUrl = null;
  } else {
    let url_parse = url.parse(requestUrl, true);
    url_parse.query.offset = offset - limit;
    url_parse.query.category = category;
    delete url_parse.search;
    nextUrl = "http://localhost:3000/articles" + url.format(url_parse);
  }
  return nextUrl;
};

exports.create_next_url = (requestUrl, limit, offset, total, category) => {
  let nextUrl;
  if (total <= offset * limit || total <= limit) {
    nextUrl = null;
  } else {
    let url_parse = url.parse(requestUrl, true);
    url_parse.query.offset = offset + limit;
    url_parse.query.category = category;
    delete url_parse.search;
    nextUrl = "http://localhost:3000/articles" + url.format(url_parse);
  }
  return nextUrl;
};

exports.published_filter = published => {
  let filterOptions = {};
  published === null ? (filterOptions["published"] = true) : null;
  return filterOptions;
};

exports.create_pev_pages_url = (requestUrl, limit, offset) => {
  let nextUrl;
  if (offset === 0) {
    nextUrl = null;
  } else {
    let url_parse = url.parse(requestUrl, true);
    url_parse.query.offset = offset - limit;
    delete url_parse.search;
    nextUrl = "http://localhost:3000/pages" + url.format(url_parse);
  }
  return nextUrl;
};

exports.create_next_pages_url = (requestUrl, limit, offset, total) => {
  let nextUrl;
  if (total <= offset * limit || total <= limit) {
    nextUrl = null;
  } else {
    let url_parse = url.parse(requestUrl, true);
    url_parse.query.offset = offset + limit;
    delete url_parse.search;
    nextUrl = "http://localhost:3000/pages" + url.format(url_parse);
  }
  return nextUrl;
};
